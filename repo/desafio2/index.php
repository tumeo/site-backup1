<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Desafio 2</title>
        <style>
            body{
                font-family: verdana;
            }
            table.anual-calendar {
                display: inline-table;
            }
            table{
                text-align: center;
                margin: 8px 5px;
            }
            th{
                padding: 2px;
            }
            fieldset{
                max-width: 940px;
            }
            span.domingo{
                color: red;
            }
            span.sabado{
                color: blue;
            }
        </style>
    </head>
    <body>

<?php

function exe($exe=''){
    if($exe == 1){
        $exe = 'Faça uma página que exiba a hora e a frase “Bom dia”, “Boa tarde” ou “Boa noite”, de acordo com a hora. Use a condicional if e a função date().';
    }elseif ($exe == 2) {
        $exe = 'Faça com que o calendário exiba o dia atual em negrito, usando a função date().';
    }elseif ($exe == 3) {
        $exe = 'Exiba os domingos em vermelho e os sábados em azul.';
    }elseif ($exe == 4) {
        $exe = 'Faça o calendário começar em um dia que não seja um domingo.';
    }elseif ($exe == 5) {
        $exe = 'E um calendário do ano todo? Será que é muito complexo?';
    }
    echo "<h3>$exe</h3>";
}

function linha($semana, $atual_month=false){
    echo "<tr>";
    for ($i = 0; $i <= 6; $i++){
        if (isset($semana[$i]) && $semana[$i] > 0){
            echo "<td>";
            if($i == 0){
                echo '<span class="domingo">';
            }elseif ($i == 6) {
                echo '<span class="sabado">';
            }else {
                echo "<span>";
            }

            if($semana[$i] == date("d") && $atual_month == date("m")){
                // desafio 2
                echo "<b> {$semana[$i]}</b>";
            }else{
                echo "{$semana[$i]}";
            }
            echo "</span></td>";
        } else {
            echo "<td></td>";
        }
    } echo "</tr>";
}

function get_calendar_init($mes=false){

    if($mes == 1){
        return -3;
    }elseif($mes == 2){
        return 1;
    }elseif($mes == 3){
        return 1;
    }elseif($mes == 4){
        return -2;
    }elseif($mes == 5){
        return -4;
    }elseif($mes == 6){
        return 0;
    }elseif($mes == 7){
        return -2;
    }elseif($mes == 8){
        return -5;
    }elseif($mes == 9){
        return -1;
    }elseif($mes == 10){
        return -3;
    }elseif($mes == 11){
        return 1;
    }elseif($mes == 12){
        return -1;
    }
}

function get_month($mes){
    if($mes == 1){
        return 'Janeiro';
    }elseif($mes == 2){
        return 'Fevereiro';
    }elseif($mes == 3){
        return 'Março';
    }elseif($mes == 4){
        return 'Abril';
    }elseif($mes == 5){
        return 'Maio';
    }elseif($mes == 6){
        return 'Junho';
    }elseif($mes == 7){
        return 'Julho';
    }elseif($mes == 8){
        return 'Agosto';
    }elseif($mes == 9){
        return 'Setembro';
    }elseif($mes == 10){
        return 'Outubro';
    }elseif($mes == 11){
        return 'Novembro';
    }elseif($mes == 12){
        return 'Dezembro';
    }
}

function calendario($mes=false){

    if(!$mes){
        $mes = date("m");
    }

    $dias_mes = 30;
    if($mes == 2){
        $dias_mes = 28;
    }elseif (   $mes == 1  ||
                $mes == 3  ||
                $mes == 5  ||
                $mes == 7  ||
                $mes == 8  ||
                $mes == 10 ||
                $mes == 12){
        $dias_mes = 31;
    }

    echo "
        <tr>
            <th>Dom</th>
            <th>Seg</th>
            <th>Ter</th>
            <th>Qua</th>
            <th>Qui</th>
            <th>Sex</th>
            <th>Sáb</th>
        </tr>
    ";

    $dia = get_calendar_init($mes);

    $semana = array();
    while ($dia <= $dias_mes){

            array_push($semana, $dia);
            if (count($semana) == 7) {
                if($mes == date('m')){
                    linha($semana, $mes);
                }else{
                    linha($semana);
                }
                $semana = array();
            }

        $dia++;
    }
    linha($semana);
}

// desafio 2
exe(2);

// desafio 3
exe(3);

// desafio 4
exe(4);

?>

    <table border="1">

        <?php
            echo '<caption>'.get_month(date("m")).'</caption>';
            calendario();
        ?>

    </table>

<?php

// desafio 1
exe(1);
$hora = Array(date("h"), date("a"));
if($hora[1] == "am" && ($hora[0] > 5 && $hora[0] < 12)){
    echo "Bom dia!";
}elseif ($hora[1] == "pm" && ($hora[0] > 12 && $hora[0] < 6)) {
    echo "Boa tarde!";
}else{
    echo "Boa noite!";
}

// desafio 5
exe(5);
?>
    <fieldset>
        <legend>Calendário Anual</legend>
<?php

    for($i = 1; $i <= 12; $i++){
        echo '<table class="anual-calendar" border="1">';
        echo "<caption>".get_month($i)."</caption>";
        calendario($i);
        echo '</table>';
    }
?>
    </fieldset>

    </body>
</html>
