/*
*   > MITHYBOARD SCRIPT
*   > VERSION: 1.0
*   > BUILD: 0531-02
*   > ALTERAÇÕES:
        > Criação do pseudo-DB
        > Função que auto-escreve os badges e a quantidade 
*
*
    >snippets

// exemplo de write em locais específicos
document.getElementById('idDaDiv').innerHTML += 'string a ser adicionada';

// exemplo de obter o número de badges do mitador (id)
document.getElementById('idDoMitador').childElementCount;

//Porras girantes on hover
   img.profile:hover{
    -webkit-animation:spin 4s linear infinite;
    -moz-animation:spin 4s linear infinite;
    animation:spin 0.5s linear infinite;
    @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
    @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
    @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); }}
  }

*
*/


function writeBadges(myth){

    for (var i = 1; i < myth.length; i++) {
        document.getElementById(myth[0]).innerHTML += " " + myth[i] + " ";
    }
    document.getElementById(myth[0] + "1").innerHTML += (myth.length - 1); 
}




//Tier
    var mythUnicorn = '<img  title="Mitador Unicorn: Raros porém participativos" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/uni.png">';
    var mythPlatinum = '<img title="Mitador Platinum" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/plat.png">';
    var mythSilver = '<img class="badgeicon" title="Mitador Silver" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/silver.png">';
    var mythLegacy = '<img class="badgeicon" title="Mitador Legacy: Veterano dos primórdios!" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/legacy.png">';
    var mythFresh = '<img title="Freshman: Mitadores Trainee" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/fresh.png">';
    var mythGuest = '<img title="Guest: Não-mitador que convive na casa." src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/guest.png">';
    var mythHonorary = '<img title="Mitador Honorary: Protegido pessoal de um Prime ou Platinum" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/honor.png">';
    var mythLurker = '<img title="Lurker: Sempre nas sombras, eventualmente se pronuncia." src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/lurk.png">';
    var mythGold = '<img title="Mitador Gold" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/gold.png">';
    var mythGhost = '<img class="badgeicon" title="Mitador Ghost: Inativo há mais de um mês." src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/ghost.png">';
    var mythPrime = '<img class="badgeicon" title="Mitador Prime: Os primeiros Mitadores, criadores da porra toda." src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/prime.png">';


//Badges    
    var jogueiro = '<img title="Jogueiros: Sempre presentes nas jogatinas míticas" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/joy.png">';
    var hangouteiro = '<img title="Hangouteiro: Sempre marcando presença nos Hangouts!" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/hang.png">';
    var desenheiro = '<img title="Desenheiro: Depictador dos melhores momentos." src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/pen.png">';
    var vassouraWielder = ' <img title="Vassoura Wielder: Já vassourou alguém em uma situação realmente necessária" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/broom.png">';
    var _3dsPlay = '<img title="3DS Teamplay: Jogou com mitadores via Nintendo 3DS" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/3ds.png">';
    var defiance = '<img title="Defiance: Sem medo de umas vassouradas" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/defiance.png">';
    var pipoqueiro = '<img title="Arthur exclusive \'Pipoqueiro Badge\'" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/pop.png">';
    var encontroMyth = '<img title="Encontro Mitador: eu fui!" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/group.png">';
    var smashFesteiro = '<img title="Smashfesteiro" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/ssb.png">';
    var smashFest2014 = '<img title="Newyear 2014: Smashfest - Eu fui!" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/2014.png">';
    var mechanicPunch = '<img title="Claro exclusive \'Mechanic Punch\' Badge" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/soc.png">';
    var programeiro = '<img title="Programeiro: Ajuda a codar paradas mitadoras" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/code.png">';
    var frukiFest = '<img class="badgeicon" title="Newyear 2013: Frukifest - Eu fui!" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/2013.png">';
    var streameiro = '<img class="badgeicon" title="Streameiro: Ocasionalmente promove Livestreams" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/live.png">';
    var gardevoa = '<img class="badgeicon" title="Flicky exclusive \'Gardevobadge\' " src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/garde.png">';
    var mitamores = '<img class="badgeicon" title="Os Mitamores: Espalhando amor entre os Mitos" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/mitamores.png">';
    var doisPotes = '<img class="badgeicon" title="Luana exclusive \'Doooois Pooootes\' Badge" src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/pots.png">';

//Crests
    var cVinicius = "<img title='\"The Piano Key\": Vinicius Crest\" 'src=\"http://os-mitadores.lucasflicky.com/files/theme/BADGES/vkps.png\">";
    var cFlicky = '<img title=\'"The Mighy Red Hood": Flicky Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/hood.png">'
    var cGors = '<img title=\'"The Bullet Head": Gors Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/gors.png">';
    var cAna = '<img class="badgeicon" title=\'"Yomiell Hankerchief": Ana Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/yomi.png">';
    var cPall = '<img class="badgeicon" title=\'"The Kawaii Stick" Pall Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/pall.png">';
    var cClaro = '<img class="badgeicon" title=\'"Com o Claro você fala ilimitado!" Claro Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/claro.png">';
    var cHerbe = '<img class="badgeicon" title=\'"A Erva da Alegria": Herbe Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/herb.png">';
    var cCesar = '<img class="badgeicon" title=\'"The Rockstar Sneakers": Rasec Crest\' src="http://os-mitadores.lucasflicky.com/files/theme/BADGES/rasec.png">';


    var mAlex = [
        'foffano',
        mythUnicorn
    ];
    var mAna = [
        'yomiell',
        mythPlatinum,
        jogueiro,
        hangouteiro,
        desenheiro,
        vassouraWielder,
        _3dsPlay,
        defiance
    ];
    var mAnaErrada = [
        'anaerrada',
        mythUnicorn,
        cVinicius
    ];
    var mAragao = [
        'aragao',
        mythLegacy,
        mythSilver,
        hangouteiro,
        vassouraWielder,
        streameiro
    ];
    var mArthur = [
        'arthur',
        mythFresh,
        cFlicky,
        jogueiro,
        pipoqueiro
    ];
    var mBruna = [
        'bruna',
        mythGuest,
        cFlicky
    ];
    var mCesar = [
        'rasec',
        mythHonorary,
        cFlicky,
        mythLurker,
        encontroMyth,
        smashFesteiro,
        smashFest2014
    ];
    var mClaro = [
        'claro',
        mythGold,
        mechanicPunch,
        hangouteiro,
        defiance,
        vassouraWielder,
        cGors,
        programeiro
    ];
    var mCoelho = [
        'coelho',
        mythLegacy,
        mythGhost,
        frukiFest,
        streameiro,
        smashFest2014,
        _3dsPlay
      ];
    var mFernando = [
        'duodyn',
        mythHonorary,
        mythGhost,
        cGors,
        hangouteiro
    ];
    var mFlicky = [
        'flike',
        mythPrime,
        desenheiro,
        jogueiro,
        hangouteiro,
        frukiFest,
        vassouraWielder,
        encontroMyth,
        gardevoa,
        streameiro,
        mitamores,
        smashFest2014,
        programeiro,
        _3dsPlay
    ];
    var mGarland = [
        'garland',
        mythHonorary,
        encontroMyth,
        mythLurker,
        cFlicky,
        smashFest2014
    ];
    var mHerbe = [
        'herbe',
        mythPrime,
        frukiFest,
        encontroMyth,
        smashFesteiro,
        vassouraWielder,
        smashFest2014,
        _3dsPlay,
        mitamores
    ];
    var mKaren = [
        'karen',
        mythLurker,
        smashFesteiro,
        encontroMyth,
        smashFest2014
    ];
    var mKeidi = [
        'keidi',
        mythUnicorn,
        cAna,
        streameiro
    ];
    var mLaise = [
        'laise',
        mythHonorary,
        mythGhost,
        mythGuest,
        cFlicky
    ];
    var mLuana = [
        'luana',
        mythHonorary,
        encontroMyth,
        doisPotes,
        cFlicky,
        frukiFest,
        mitamores
    ];
    var mMarcela = [
        'marcela',
        mythLurker,
        cFlicky
    ];
    var mMatsuna = [
        'matsuna',
        mythPrime,
        mythGhost
    ];
    var mPall = [
        'pall',
        mythLegacy,
        mythLurker,
        desenheiro,
        mitamores,
        jogueiro,
        hangouteiro,
        streameiro,
        _3dsPlay
    ];
    var mPam = [
        'pam',
        mythFresh,
        cPall,
        defiance
    ];
    var mRubens = [
        'rubens',
        mythUnicorn,
        cClaro
    ];
    var mTumeo = [
        'tunel',
        mythGold,
        programeiro
    ];
    var mVidal = [
        'vidal',
        mythSilver,
        encontroMyth,
        cHerbe,
        smashFesteiro,
        smashFest2014,
        _3dsPlay
    ];
    var mVinicius = [
        'vinicius',
        mythLegacy,
        mythUnicorn,
        jogueiro,
        frukiFest,
        hangouteiro,
        encontroMyth,
        smashFesteiro,
        smashFest2014,
        _3dsPlay
    ];
    var mGors = [
        'gors',
        mythLegacy,
        mythPlatinum,
        desenheiro,
        jogueiro,
        frukiFest,
        hangouteiro,
        encontroMyth,
        streameiro,
        smashFesteiro,
        _3dsPlay,
        smashFest2014
    ];
    var mWolvie = [
        'wolvie',
        mythFresh,
        cCesar
    ];


writeBadges(mAlex);
writeBadges(mAna);
writeBadges(mAnaErrada);
writeBadges(mAragao);
writeBadges(mArthur);
writeBadges(mBruna);
writeBadges(mCesar);
writeBadges(mClaro);
writeBadges(mCoelho);
writeBadges(mFernando);
writeBadges(mFlicky);
writeBadges(mGarland);
writeBadges(mHerbe);
writeBadges(mKaren);
writeBadges(mKeidi);
writeBadges(mLaise);
writeBadges(mLuana);
writeBadges(mMarcela);
writeBadges(mMatsuna);
writeBadges(mPall);
writeBadges(mPam);
writeBadges(mRubens);
writeBadges(mTumeo);
writeBadges(mVidal);
writeBadges(mVinicius);
writeBadges(mGors);
writeBadges(mWolvie);

