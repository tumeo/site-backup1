<?php include '../../libs/session.php'; ?>

<html>

<?php
    $level = 'Listar';
    include '../../libs/head_meta.php';
    include "../conecta_mysql.inc";
?>

    <body>
        <?php include '../menu.php'; ?>
        <h2 align="center">Lista de Produtos</h2>
        <hr>
        <div align="center">
            <center>
                <table border="1" cellpadding="0" cellspacing="0" width="90%">
                    <tr>
                        <td width="10%" bgcolor="#EEEEEE" align="center">
                            <p align="center"><b><a href="./?ordem=codigo_produto">Código</a></b></td>
                        <td width="25%" bgcolor="#EEEEEE" align="center"><b><a href="./?ordem=nome_produto">Nome</a></b></td>
                        <td width="40%" bgcolor="#EEEEEE" align="center"><b><a href="./?ordem=descricao_produto">Descriçao</a></b></td>
                        <td width="14%" bgcolor="#EEEEEE" align="center"><b><a href="./?ordem=preco">Preço</a></b></td>
                        <td width="11%" bgcolor="#EEEEEE" align="center"><b><a href="./?ordem=cod_categoria">Categoria</a></b></td>
                    </tr>
                    <?php

	if(isset($_GET["ordem"]))
		$ordem = $_GET["ordem"];
	else
		$ordem = "codigo_produto";
	$sql = "SELECT * FROM produtos ORDER BY $ordem";
	$res = mysql_query($sql);
	while($registro=mysql_fetch_row($res))
	{
		$codigo=$registro[0];
		$nome=$registro[1];
		$descricao=$registro[2];
		$preco=number_format($registro[3],2,",",".");
		$categoria=$registro[4];
    ?>
	   <tr>
		<td width='10%'>
		<p align='center'><?php echo $codigo ?></td>
		<td width='25%'><?php echo $nome ?></td>
		<td width='40%'><?php echo $descricao ?></td>
		<td width='14%' align='center'>R$<?php echo $preco ?></td>
		<td width='11%' align='center'><?php echo $categoria ?></td>
		</tr>
    <?php
	}
	mysql_close($conexao);
?>
                </table>
            </center>
        </div>
        <p align="center"><a href="../">Voltar</a></p>
    </body>

</html>
