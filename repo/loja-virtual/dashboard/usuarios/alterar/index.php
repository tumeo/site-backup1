<?php
    $check_session = 'adm';
    include '../../../libs/session.php';
?>

<html>

<?php
    $level = 'Usuários - Alterar';
    include '../../../libs/head_meta.php';
    include "../../conecta_mysql.inc";

    $users = [];
    $select_sql = "SELECT * FROM usuario";
    $select_result = mysql_query($select_sql);
    while($usr = mysql_fetch_assoc($select_result)){
        $users[$usr['id']] = $usr;
    }
?>

    <body>
        <?php
            include '../../menu.php';
        ?>
        <h2 align="center">Alterar usuário</h2>
        <hr>

        <ul>
            <select name="select-user">
                <option value="none">Selecione um usuário</option>
                <?php foreach ($users as $useri): ?>
                    <option value="<?php echo $useri['id'] ?>"><?php echo $useri['usuario'] ?></option>
                <?php endforeach; ?>
            </select>
        </ul>

        <?php if (isset($_GET["usuario"])): ?>
        <?php $usuario = $users[$_GET["usuario"]]; ?>

            <h3><?php echo $usuario['usuario'] ?></h3>
            <form class="new-user" action="insert_user.php" method="post">
                <ul>
                    Usuário
                    <ul>
                        <input required type="text" name="user" value="<?php echo $usuario['usuario'] ?>">
                    </ul>
                </ul>
                <ul>
                    Email
                    <ul>
                        <input required type="email" name="email" value="<?php echo $usuario['email'] ?>">
                    </ul>
                </ul>
                <ul>
                    Tipo
                    <ul>
                        <select name="tipo">
                            <option <?php echo $usuario['tipo'] == 'uc' ? 'selected' : '' ?> value="uc">Usuário comum</option>
                            <option <?php echo $usuario['tipo'] == 'su' ? 'selected' : '' ?> value="su">Super usuário</option>
                            <option <?php echo $usuario['tipo'] == 'adm' ? 'selected' : '' ?> value="adm">Administrador</option>
                        </select>
                    </ul>
                </ul>
                <ul>
                    Senha
                    <ul>
                        <input required type="password" name="senha" value="<?php echo $usuario['senha'] ?>">
                    </ul>
                </ul>
                <ul>
                    Confirmar senha
                    <ul>
                        <input required type="password" name="senha-valid" value="<?php echo $usuario['senha'] ?>">
                    </ul>
                </ul>
                <ul>
                    <button type="submit" name="alter">Alterar</button>
                </ul>
                <input type="hidden" name="id" value="<?php echo $usuario['id'] ?>">
            </form>
        <?php endif; ?>
        <div class="message">
        </div>
        <p align="center"><a href="../">Voltar</a></p>
    </body>
    <script src="/loja-virtual/libs/scripts/alterar.js" charset="utf-8"></script>
</html>
