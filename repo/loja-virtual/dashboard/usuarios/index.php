<?php include '../../libs/session.php'; ?>

<html>

<?php
    $level = 'Usuários';
    include '../../libs/head_meta.php';
    include "../conecta_mysql.inc";
?>

    <body>
        <?php include '../menu.php'; ?>
        <h2 align="center">Usuários</h2>
        <hr>
        <center>
            <a href="./novo/"><b>Novo usuário</b></a><br>
            <?php if ($_SESSION['tipo'] == 'adm'): ?>
                <a href="./alterar/"><b>Alterar usuário</b></a><br>
                <a href="./excluir/"><b>Excluir usuário</b></a><br>
                <a href="./listar/"><b>Listar usuários</b></a><br>
            <?php endif; ?>
            <p align="center"><a href="../">Voltar</a></p>
        </center>
    </body>

</html>
