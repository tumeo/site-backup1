<?php
    $check_session = 'adm';
    include '../../../libs/session.php';
?>

<html>

<?php
    $level = 'Usuários - Excluir';
    include '../../../libs/head_meta.php';
    include "../../conecta_mysql.inc";

    $users = [];
    $select_sql = "SELECT * FROM usuario";
    $select_result = mysql_query($select_sql);
    while($usr = mysql_fetch_assoc($select_result)){
        $users[$usr['id']] = $usr;
    }
?>

    <body>
        <?php
            include '../../menu.php';
        ?>
        <h2 align="center">Excluir usuário</h2>
        <hr>

        <ul>
            <select name="select-user">
                <option value="none">Selecione um usuário</option>
                <?php foreach ($users as $useri): ?>
                    <option value="<?php echo $useri['id'] ?>"><?php echo $useri['usuario'] ?></option>
                <?php endforeach; ?>
            </select>
        </ul>

        <?php if (isset($_GET['usuario'])): ?>
            <?php $usuario = $users[$_GET['usuario']]; ?>
            <h3>Excluir <i><?php echo $usuario['usuario'] ?></i>?</h3>
        <?php endif; ?>

        <button type="button" name="excluir">Excluir</button>
        <input type="hidden" name="id" value="<?php echo $usuario['id'] ?>">
        <div class="message">
        </div>
        <p align="center"><a href="../">Voltar</a></p>
    </body>
    <script src="/loja-virtual/libs/scripts/excluir.js" charset="utf-8"></script>
</html>
