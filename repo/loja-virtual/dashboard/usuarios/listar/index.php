<?php

$check_session = 'adm';
include '../../../libs/session.php';
include "../../conecta_mysql.inc";

$ordem = 'id';

if(isset($_GET['id'])){
    $ordem = 'id';
}elseif (isset($_GET['user'])) {
    $ordem = 'usuario';
}elseif (isset($_GET['email'])) {
    $ordem = 'email';
}elseif (isset($_GET['tipo'])) {
    $ordem = 'tipo';
}

$select_script = "SELECT * FROM usuario ORDER BY $ordem";
$select_sql = mysql_query($select_script);

function parseType($tipo){
    $tipos = [
        'uc' => 'Usuário comum',
        'su' => 'Super usuário',
        'adm' => 'Administrador'
    ];
    return $tipos[$tipo];
}

?>

<html>

<?php
    $level = 'Usuários - Listar';
    include '../../../libs/head_meta.php';
?>

    <body>
        <?php include '../../menu.php'; ?>
        <h2 align="center">Listar usuários</h2>
        <hr>
        <center>
            <table border="1" cellpadding="0" cellspacing="0" width="60%">
                <thead>
                    <th>
                        <a href="./?id">ID</a>
                    </th>
                    <th>
                        <a href="./?user">Usuário</a>
                    </th>
                    <th>
                        <a href="./?email">Email</a>
                    </th>
                    <th>
                        <a href="./?tipo">Tipo</a>
                    </th>
                </thead>
                <tbody>
                    <?php
                        while ($user = mysql_fetch_assoc($select_sql)) {
                    ?>
                    <tr>
                        <td align='center'>
                            <?php echo $user['id']; ?>
                        </td>
                        <td align='center'>
                            <?php echo $user['usuario']; ?>
                        </td>
                        <td align='center'>
                            <?php echo $user['email']; ?>
                        </td>
                        <td align='center'>
                            <?php echo parseType($user['tipo']); ?>
                        </td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
            <p align="center"><a href="../">Voltar</a></p>
        </center>
    </body>

</html>
