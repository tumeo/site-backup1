<?php
    $user_type = 'uc';
    include '../../../libs/session.php';

    if(isset($_SESSION['logged'])){
        $user_type = $_SESSION['tipo'];
    }
?>

<html>

<?php
    $level = 'Usuários - Novo';
    include '../../../libs/head_meta.php';
?>

    <body>
        <?php
            if (isLogged()) {
                include '../../menu.php';
            }
        ?>
        <h2 align="center">Novo usuário</h2>
        <hr>
        <form class="new-user" action="insert_user.php" method="post">
            <ul>
                Usuário
                <ul>
                    <input required type="text" name="user" value="">
                </ul>
            </ul>
            <ul>
                Email
                <ul>
                    <input required type="email" name="email" value="">
                </ul>
            </ul>
            <ul>
                Tipo
                <ul>
                    <select name="tipo">
                        <option value="uc">Usuário comum</option>
                    <?php if($user_type != 'uc'){ ?>
                        <option value="su">Super usuário</option>
                        <?php if($user_type == 'adm'){ ?>
                        <option value="adm">Administrador</option>
                    <?php }   } ?>
                    </select>
                </ul>
            </ul>
            <ul>
                Senha
                <ul>
                    <input required type="password" name="senha" value="">
                </ul>
            </ul>
            <ul>
                Confirmar senha
                <ul>
                    <input required type="password" name="senha-valid" value="">
                </ul>
            </ul>
            <ul>
                <button type="submit" name="novo">Cadastrar</button>
            </ul>
        </form>
        <div class="message">
        </div>
        <p align="center"><a href="../">Voltar</a></p>
    </body>
    <script src="/loja-virtual/libs/scripts/novo.js" charset="utf-8"></script>
</html>
