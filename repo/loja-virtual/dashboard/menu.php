
<?php
    $tipo_usuario = [
        'uc' => 'Usuário comum',
        'su' => 'Super usuário',
        'adm' => 'Administrador'
    ];
?>

<div class="menu">

    <div class="header">
        <a href="<?php echo PATH ?>/dashboard/"><b>Painel da Loja Tabajara</b></a>
    </div>
    <div class="nav">
        <?php if ($_SESSION['tipo'] != 'su'): ?>
        <a href="<?php echo PATH ?>/dashboard/incluir/"><b>Incluir Produto</b></a>
        <?php endif; ?>

        <a href="<?php echo PATH ?>/dashboard/alterar/"><b>Alterar produto</b></a>

        <?php if ($_SESSION['tipo'] != 'uc'): ?>
            <a href="<?php echo PATH ?>/dashboard/excluir/"><b>Excluir produtos</b></a>
        <?php endif; ?>

        <a href="<?php echo PATH ?>/dashboard/listar/"><b>Listar produtos</b></a>

        <?php if ($_SESSION['tipo'] != 'uc'): ?>
            <a href="<?php echo PATH ?>/dashboard/usuarios/"><b>Usuários</b></a>
        <?php endif; ?>
        
        <a href="<?php echo PATH ?>/logout/"><b>Sair</b></a>
    </div>

    <hr>


</div>

<div class="usuario">
    <b><?php echo $_SESSION['user']; ?></b> |
    <b><?php echo $tipo_usuario[$_SESSION['tipo']]; ?></b>
</div>
