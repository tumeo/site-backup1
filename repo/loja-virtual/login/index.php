<?php

    $check_session = 'login';

    include '../libs/session.php';

    $level = 'Login';

?>

<!DOCTYPE html>
<html>

    <?php include '../libs/head_meta.php'; ?>

    <body>
        <div class="main-content">
            <h1>Loja Virtual | Login</h1>
            <div class="message">

            </div>
            <form class="login" method="post">
                <label for="user">Usuário</label>
                <ul>
                    <input required type="user" name="user" value="" placeholder="Digite seu usuário">
                </ul>
                <label for="user">Senha</label>
                <ul>
                    <input required type="password" name="password" value="" placeholder="Digite sua senha">
                </ul>
                <button type="submit" name="login">Entrar</button>
                <button type="button" name="novo-user">Novo usuário</button>
            </form>

        </div>
    </body>
    <script src="/loja-virtual/libs/scripts/login.js" charset="utf-8"></script>
</html>
