<?php
    
    // PATH
    define('PATH', '/loja-virtual');

    session_start();
    $check_session = isset($check_session) ? $check_session : 'check';

    // Main session check
    function isLogged(){
        return isset($_SESSION['logged']);
    }

    // Main session redirect
    function checkSession(){
        if(!isLogged()){
            header('Location: '.PATH.'/login/');
            exit();
        }
    }

    function checkAdm(){
        if($_SESSION['tipo'] != 'adm'){
            header('Location: '.PATH.'/dashboard/');
            exit();
        }
    }

    function logIn(){
        if(isset($login_safe)) {
            $_SESSION['logged'] = true;
            $_SESSION['user'] = $_POST['user'];
            header('Location: '.PATH.'/dashboard/');
            exit();
        }elseif (isLogged()){
            header('Location: '.PATH.'/dashboard/');
            exit();
        }
    }

    // Main session logout
    function logOut(){
        unset($_SESSION['logged']);
        session_destroy();
        checkSession();
    }

    // Main
    switch ($check_session){
        case 'check':
            if(!isset($user_type)){
                checkSession();
            }
            break;
        case 'logout':
            logOut();
            break;
        case 'login':
            logIn();
            break;
        case 'adm':
            checkSession();
            checkAdm();
    }

?>
