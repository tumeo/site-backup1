$('select[name="select-user"]').change(function () {
    if(this.value !== 'none'){
        location.href = './?usuario='+this.value;
    }
});

function showMessagem(message){
    var messageEl = document.querySelector('.message');
    messageEl.innerHTML = message;
    $(messageEl).show('fast');
}

if(location.search.match(/usuario/)){
    $('button').click(function () {
        var input = document.querySelector('input');
        var post = {};
        post[this.name] = this.value;
        post[input.name] = input.value;
        $('.message').hide('fast');
        $.post('excluir.php', post,
            function (result) {
                switch (result) {
                    case 'success':
                        showMessagem("Usuário excluído com sucesso!");
                        break;
                    case 'error':
                        showMessagem("Algum erro ocorreu ao excluir!");
                        break;
                    default:
                        showMessagem("Erro desconhecido!"+result);
                }
            }
        );
        return false;
    });
}
