$('select[name="select-user"]').change(function () {
    if(this.value !== 'none'){
        location.href = './?usuario='+this.value;
    }
});

function showMessagem(message){
    var messageEl = document.querySelector('.message');
    messageEl.innerHTML = message;
    $(messageEl).show('fast');
}

if(location.search.match(/usuario/)){
    $('form').submit(function () {
        if ($('input[type=password]')[0].value !== $('input[type=password]')[1].value) {
            showMessagem("Senhas não conferem!");
            return false;
        }
        var inputs = document.querySelectorAll('input');
        var select = document.querySelectorAll('select')[1];
        var button = document.querySelector('button');
        var post = {};
        post[select.name] = select.value;
        post[button.name] = button.value;
        for (var i = 0; i < inputs.length; i++) {
            post[inputs[i].name] = inputs[i].value;
        }
        $('.message').hide('fast');

        $.post('update.php', post,
            function (result) {
                switch (result) {
                    case 'success':
                        showMessagem("Alteração efetuada com sucesso!");
                        break;
                    case 'error':
                        showMessagem("Algum erro ocorreu na alteração!");
                        break;
                    case 'limit':
                        showMessagem("Limite de administradores atingido!");
                        break;
                    default:
                        showMessagem("Erro desconhecido!"+result);
                }
            }
        );
        return false;
    });
}
