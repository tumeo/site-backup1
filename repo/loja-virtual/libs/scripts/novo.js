function showMessagem(message){
    var messageEl = document.querySelector('.message');
    messageEl.innerHTML = message;
    $(messageEl).show('fast');
}

$('form').submit(
    function(clickEvent){
        if ($('input[type=password]')[0].value !== $('input[type=password]')[1].value) {
            showMessagem("Senhas não conferem!");
            return false;
        }
        var inputs = document.querySelectorAll('input');
        var select = document.querySelector('select');
        var button = document.querySelector('button');
        var post = {};
        post[select.name] = select.value;
        post[button.name] = button.value;
        for (var i = 0; i < inputs.length; i++) {
            post[inputs[i].name] = inputs[i].value;
        }
        $('.message').hide('fast');
        $.post('insert_user.php', post,
            function (result) {
                switch (result) {
                    case 'success':
                        showMessagem("Cadastro efetuado com sucesso!");
                        break;
                    case 'error':
                        showMessagem("Algum erro ocorreu ao cadastrar!");
                        break;
                    case 'limit':
                        showMessagem("Limite de administradores atingido!");
                        break;
                    default:
                        showMessagem("Erro desconhecido!"+result);
                }
            }
        );
        return false;
    }
);
