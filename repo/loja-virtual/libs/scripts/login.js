function showMessagem(message){
    var messageEl = document.querySelector('.message');
    switch (message) {
        case 'user':
            messageEl.innerHTML = 'Usuário inválido!';
            break;
        case 'pass':
            messageEl.innerHTML = 'Senha inválida!';
            break;
        default:
            messageEl.innerHTML = 'Erro desconhecido!';
    }
    $(messageEl).show('fast');
}


$('form').submit(
    function(clickEvent){
        var user = document.querySelector('input[name=user]');
        var pass = document.querySelector('input[name=password]');
        var post = {};
        post[user.name] = user.value;
        post[pass.name] = pass.value;
        $('.message').hide('fast');
        $.post('auth.php', post,
            function (result) {
                switch (result) {
                    case 'invalid_user':
                        showMessagem('user');
                        break;
                    case 'ivalid_pass':
                        showMessagem('pass');
                        break;
                    case 'success':
                        location.reload();
                        break;
                    default:
                        showMessagem('error');
                }
            }
        );
        return false;
    }
);

$('button[name="novo-user"]').click(function (clickEvent) {
    location.href = '../dashboard/usuarios/novo/';
});
