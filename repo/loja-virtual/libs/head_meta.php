<?php
    //include_once '../libs/session.php';

    $project = "Loja Virtual";
    $level = isset($level) ? ' | '.$level : '';

    function setTitle($project_, $level_){
        ?>
        <title>
            <?php echo $project_.$level_; ?>
        </title>
        <?php
    }
?>

<head>
    <meta charset="utf-8">
    <?php setTitle($project, $level) ?>
    <script src="<?php echo PATH ?>/libs/scripts/jquery-1.9.1.min.js" charset="utf-8"></script>
    <link rel="stylesheet" href="<?php echo PATH ?>/libs/styles/dashboard.css" media="screen">
</head>
