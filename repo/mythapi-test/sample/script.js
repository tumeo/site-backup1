var loading;
var contentLoaded = 0;
var PATH = 'http://williamd1k0.github.io/mythapi-test/';
var myth_data;
var myths;


ChainImport(PATH + 'core/sql.js', function(event){
    
    ChainImport(PATH + 'core/DataBase.js', function(event){
        myth_data = new DataBase('./', 'myth.db');
        contentLoaded++;
    });

    ChainImport(PATH + 'core/Myth.js', function(event){
        contentLoaded++;
    });
    
});


window.onload = function(){
    
    // Main controller
    loading = setInterval(function(){
        if(contentLoaded >= 2 && myth_data){
            clearInterval(loading);
            
            try{
                document.querySelector('progress').style.display = 'none';
                document.querySelector('#result').style.visibility = 'visible';
            }catch(e){}
            
            myths = myth_data.select('select * from myths');
            try{
                document.querySelector('#result').innerHTML = var_dump(myths);
            }catch(e){}
            
            MythAPI.new('myth', {'myths':myths});
        
        }
    },1000);
    
}

window.onbeforeunload = function(){
    myth_data.close();
}


function var_dump(varArray){
    var html_content = '<table border="1">';
    html_content += '<th>ID</th>';
    html_content += '<th>Nome</th>';
    html_content += '<th>Idade</th>';

    for(var i = 0; i < varArray.length; i++){
        html_content += '<tr>';
        html_content += '<td>'+varArray[i]['id']+'</td>';
        html_content += '<td>'+varArray[i]['nome']+'</td>';
        html_content += '<td>'+varArray[i]['idade']+'</td>';
        html_content += '</tr>';
    }
    html_content += '</table>';
    return html_content;
}
