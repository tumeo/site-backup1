function MythAPI_Exception(type, args){
    if(!isUndefined(type)){
        if(type === 'noid'){
            console.warn('%s%c%s','The parameter ', 'color:#159','id','is required!');
        }else if (type === 'noparams') {
            console.warn('%s%c%s','The parameter ', 'color:#159','params','is required!');
        }else if (type === 'noobject') {
            console.warn('%s%c%s','The parameter ', 'color:#159','params','must be an object!');
        }else if (type === 'idexists') {
            console.warn('%s%c%s','The id ', 'color:#159', args,'already exists!');
        }
    }else{
        console.warn('%s%c%s','The ', 'color:#159', 'MythAPI','is a static class!');
    }
    this.name = "MythAPI Exception";
    this.message = "An error with MythAPI! Please read the warning";
}

MythAPI_Exception.prototype = Error.prototype;