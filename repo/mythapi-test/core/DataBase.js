/*
| Class DataBase
|   -> Dependencies: sql.js
|   -> Constructor:
|       * string path: path for database file
|       * string base: SQLite file name
|   -> Methods:
|       * getBase: return _db (instance of SQL.Database)
|       * close: return _db.close (close the database)
|       * listTables: return all tables
|       * select: return query select as array of objects
*/
function DataBase(path, base){
    
    var _db;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', path + base, true);
    xhr.responseType = 'arraybuffer';

    xhr.onload = function(e) {
        var uInt8Array = new Uint8Array(this.response);
        _db = new SQL.Database(uInt8Array);    
    }
    
    xhr.send();
    
    this.getBase = function(){
        return _db;
    }
    
    this.close = function(){
        _db.close();
    }
    
    this.listTables = function(){
        return this.select('select * from sqlite_master');
    }
    
    this.select = function(query){
        if(!query){
            throw new Error('Invalid query!');
        }
        query = _db.exec(query);
        query = query[0];
        
        var queryObjects = [];
        var keys = query.columns;
        var values = query.values;
        
        for(var i = 0; i < values.length; i++){
            var valueObject = {};
            for(var j = 0; j < keys.length; j++){
                valueObject[keys[j]] = values[i][j];
            }
            queryObjects.push(valueObject);
        }
        return queryObjects;
    }
    

}