/*!
 * MythAPI JavaScript Library v0.1.0
 * https://github.com/thetimetunnel
 *
 * Copyright 2015, TheTimeTunnel
 * Released under the GNU GPL v2 license
 * http://
 *
 * Date: 2015-11-7
 */


function isUndefined(variable){
    return typeof variable === 'undefined';
}

function isObject(variable){
    return variable instanceof Object && !(variable instanceof Array) && typeof variable !== 'function';
}

function isFunction(variable){
    return typeof variable === 'function';
}


function MythAPI_Exception(type, args){
    if(!isUndefined(type)){
        if(type === 'noid'){
            console.warn('%s%c%s','The parameter ', 'color:#159','id','is required!');
        }else if (type === 'noparams') {
            console.warn('%s%c%s','The parameter ', 'color:#159','params','is required!');
        }else if (type === 'noobject') {
            console.warn('%s%c%s','The parameter ', 'color:#159','params','must be an object!');
        }else if (type === 'idexists') {
            console.warn('%s%c%s','The id ', 'color:#159', args,'already exists!');
        }
    }else{
        console.warn('%s%c%s','The ', 'color:#159', 'MythAPI','is a static class!');
    }
    this.name = "MythAPI Exception";
    this.message = "An error with MythAPI! Please read the warning";
}

MythAPI_Exception.prototype = Error.prototype;


function ChainImport(source, isAsync, isDefer, on_load){
    
    if(isFunction(isAsync)){
        on_load = isAsync;
        isAsync = false;
    }else if(isFunction(isDefer)){
        on_load = isDefer;
        isDefer = false;
    }
    
    isAsync = isAsync || false;
    isDefer = isDefer || false;

    var script = document.createElement('script');
    script.src = source;
    script.async = isAsync;
    script.defer = isDefer;
    script.onload = on_load;
    document.head.appendChild(script);
}

function MythAPI(){
    throw new MythAPI_Exception();
}

MythAPI.list = [];

MythAPI.getMyth = function(id){
    return this.list.indexOf(id) >= 0 ? window[id] : undefined;
}

MythAPI.new = function(id, params){
    function MythBase(id, params){
        var _id = id;
        var _name = params.name || "Unnamed";
        var _badges = params.badges || [];
        var _myths = params.myths || [];
        var _tiers = params.tiers || [];
        var _crests = params.crests || [];

        this.getId = function(){
            return _id;
        }
        this.getName = function(){
            return _name;
        }
        this.setName = function(newName){
            _name = newName;
        }
        this.getMyths = function(){
            return _myths;
        }
        this.getBadges = function(){
            return _badges;
        }
        this.getTiers = function(){
            return _tiers;
        }
        this.getCrests = function(){
            return _crests;
        }

        this.select = null;

    }
    
    if(!isUndefined(id) && id !== ""){
        if(!isUndefined(params)){
            if(isObject(params)){
                if(this.list.indexOf(id) === -1){
                    window[id] = new MythBase(id, params);
                    this.list.push(id);
                }else{
                    throw new MythAPI_Exception('idexists',id);
                }
            }else{
                throw new MythAPI_Exception('noobject');
            }
        }else{
            throw new MythAPI_Exception('noparams');
        }
    }else{
        throw new MythAPI_Exception('noid');
    }
}
