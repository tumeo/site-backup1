function MythBase(id, params){
    
    var _id = id;
    var _name = params.name || "Unnamed";
    var _badges = params.badges || [];
    var _myths = params.myths || [];
    var _tiers = params.tiers || [];
    var _crests = params.crests || [];

    this.getId = function(){
        return _id;
    }
    this.getName = function(){
        return _name;
    }
    this.setName = function(newName){
        _name = newName;
    }
    this.getMyths = function(){
        return _myths;
    }
    this.getBadges = function(){
        return _badges;
    }
    this.getTiers = function(){
        return _tiers;
    }
    this.getCrests = function(){
        return _crests;
    }

    this.select = null;

}