<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Desafio 1</title>
    </head>
    <body>

<?php

function exe($exe=''){
    if($exe == 1){
        $exe = 'Na função date(), experimente mudar o Y para y. O que acontece?';
    }elseif ($exe == 2) {
        $exe = 'Você consegue exibir a hora no formato de 12 horas, am e pm?';
    }elseif ($exe == 3) {
        $exe = 'E se você tivesse que exibir o dia da semana? Como faria?';
    }elseif ($exe == 4) {
        $exe = 'Exiba quantos dias faltam para o próximo sábado. Por exemplo, se hoje for quarta, então faltam 3 dias para sábado.';
    }elseif ($exe == 5) {
        $exe = 'Exiba também o nome do mês atual.';
    }
    echo "<h3>$exe</h3>";
}

// desafio 1
exe(1);
    echo date("y");

// desafio 2
exe(2);
    echo date("h:i:s a");

// desafio 3
exe(3);
    echo date("N l");

// desafio 4
exe(4);
    $dia = date("w");
    $falta = 6 - $dia;
    if($falta > 1){
        echo "Faltam $falta dias para sábado";
    }elseif ($falta == 1){
        echo "Amanhã é sábado";
    }else {
        echo "Hoje é sábado!";
    }

// desafio 5
exe(5);
    echo date("F");
    
?>


    </body>
</html>
