<?php
  error_reporting(0);

  function show_input($name,$value="",$type="number"){
    echo "<input type=\"$type\" name=\"$name\" placeholder=\"Digite um número\" value=\"$value\" required>";
  }

  function def_input($postname){
    if(isset($_REQUEST[$postname])){
      show_input($postname,$_REQUEST[$postname]);
    }else{
      show_input($postname);
    }
  }

  function get_delta($xa,$xb,$xc){
    return pow($xb,2) - (4 * $xa * $xc);
  }

  function bhaskara($xa,$xb,$xc){
    $delta = get_delta($xa, $xb, $xc);
    if($delta > 0){
      return duas_raizes($xa, $xb, $delta);
    }elseif($delta < 0){
      return "A equação não possui raízes reais.";
    }else{
      return uma_raiz($xa, $xb);
    }
  }

  function duas_raizes($xa, $xb, $delta){
    $xx1 = (-$xb + sqrt($delta)) / 2 * $xa;
    $xx2 = (-$xb - sqrt($delta)) / 2 * $xa;
    return " x'= ".$xx1."; x''= ".$xx2;
  }

  function uma_raiz($xa, $xb){
    return -$xb / 2 * $xa;
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Calculadora Bhaskara</title>
  </head>
  <body>

    <div class="content">

      <h2>Calculadora de Bhaskara</h2>

      <form class="bhaskara" action="./" method="post">

        <ul>Valor de A:
          <?php def_input("xa"); ?>
        </ul>

        <ul>Valor de B:
          <?php def_input("xb"); ?>
        </ul>

        <ul>Valor de C:
          <?php def_input("xc"); ?>
        </ul>
        <ul>
          <button type="submit">Calcular</button>
          <button type="button" name="clear">Limpar</button>
        </ul>
      </form>

      <div class="result">
        <ul>
        <?php
          if( $_REQUEST["xa"] != "" &&
              $_REQUEST["xb"] != "" &&
              $_REQUEST["xc"] != "" ){
            echo "Resultado:";
            echo bhaskara($_REQUEST["xa"],$_REQUEST["xb"],$_REQUEST["xc"]);
          }else{
            echo "Digite os valores";
          }
        ?>
        </ul>
      </div>

    </div>

  </body>

  <script>
    var clear = document.querySelector('button[name="clear"]'),
        inputs = document.getElementsByTagName('input');
    clear.onclick = function(){
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
      }
    }
  </script>

</html>
