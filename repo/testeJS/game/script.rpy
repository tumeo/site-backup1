﻿label main_menu:
    return

label start:
    $ imgs = {}
    
label loop:
    # uncomment next line to see everything is fine if init happens
    # before first screen call
    #$ init_t()
    
    call screen browser
    
    $ init_t()
    
    jump loop

init python:
    # after this called, dynamic nested imagemap is shown
    def init_t():
        renpy.store.imgs = {
            'ground' : "image-ground.png",
            'hover' : "image-hover.png"
        }

screen browser:
    imagemap:
        ground "browser-ground.png"
        hover "browser-hover.png"
        hotspot (  0,   0,  65,  65) action Return()
        
        # in fact, viewport is not necessary, bug occurs anyway
        viewport:
            area (10, 90, 1004, 540)
            if imgs:
                imagemap:
                    ground imgs['ground']
                    hover imgs['hover']
                    hotspot (10, 10, 200, 200) action Return()
            else:
                text "click on \"<\" to see broken imagemap; on next click it gets fixed somehow"
