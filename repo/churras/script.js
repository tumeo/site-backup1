var churras = new function(){
    this.men = document.querySelector(`input[name="men"]`);
    this.women = document.querySelector(`input[name="women"]`);
    this.meat = document.querySelector(`input[name="meat"]`);
    this.beer = document.querySelector(`input[name="beer"]`);
    this.calcButton = document.getElementsByTagName(`button`)[0];
    this.clearButton = document.getElementsByTagName(`button`)[1];
    this.resultMen = document.querySelector(`input[name="result-men"]`);
    this.resultWomen = document.querySelector(`input[name="result-women"]`);
    this.result = document.getElementById('result');
    this.close = document.getElementById('close');
};

setMinValue('men');
setMinValue('women');
setMinValue('meat');
setMinValue('beer');

setClick('calcButton',calc);
setClick('clearButton',clearInputs);
setClick('close',function(){
    $(churras.result).hide('fast');
     clearInputs();
});

function calc(){
    var result = {};

    result.menMeat = 0.5 * getValue('meat') * getValue('men');
    result.womenMeat = 0.3 * getValue('meat') * getValue('women');
    result.menBeer = 5 * getValue('beer') * getValue('men');
    result.womenBeer = 2 * getValue('beer') * getValue('women');

    if(areNum(result)){
        showResult(result);
    }else{
        $(churras.result).hide();
        $(`#fail`).show('fast');
    }
}

function showResult(result){
    churras.resultMen.value = `R$ ${(result.menMeat + result.menBeer).toFixed(2)}`;
    churras.resultWomen.value = `R$ ${(result.womenMeat + result.womenBeer).toFixed(2)}`;
    $(churras.result).show('fast');
}

function areNum(result){
    var bool = true;
    for (var i in result) {
        bool = isNaN(result[i]) ? false : bool;
    }
    return bool;
}

function getValue(el,type){
    return type == 'int' ? parseInt(churras[el].value) : parseFloat(churras[el].value);
}

function clearInputs(){
    churras.men.value='';
    churras.women.value='';
    churras.meat.value='';
    churras.beer.value='';
}

function setClick(el, func){
    churras[el].onclick = func;
}

function setMinValue(el,min){
    min = min | 0;
    churras[el].onchange = function(){
        if(this.value<min){
            this.value = min;
        }
    }
    churras[el].onfocus = function(){
        $(churras.result).hide('fast');
        $(`#fail`).hide('fast');
    }
}
