## Churrascômetro

Desenvolva uma página para Web para cálculo de valor a ser cobrado em Churrasco entre amigos, cujo script seja executado na máquina cliente, conforme figura a seguir e com a seguinte especificação para o cálculo:

* Os homens comem em média meio quilo de carne e bebem 5 cervejas.
* As mulheres comem em média trezentos gramas de carne e bebem 2 cervejas.

Ao pressionar Calcular, uma ` div ` é apresentada com o resultado do valor a pagar para homens e mulheres.

> Quando a div é fechada os valores de todos os campos são zerados.

![Exemplo](https://williamd1k0.github.io/churras/image.png)