<?php
    function calc($op,$num1,$num2){
        if ($op == "+") {
            $result = $num1 + $num2;
        }elseif ($op == "-") {
            $result = $num1 - $num2;
        }elseif ($op == "*") {
            $result = $num1 * $num2;
        }elseif ($op == "/") {
            $result = $num2 != 0 ? $num1 / $num2 : "<span class=\"red\">Não é possível efetuar divisão por zero!</span>";
        }
        echo $result;
    }

    $num1 = $_REQUEST["x"];
    $num2 = $_REQUEST["y"];
    $op = $_REQUEST["op"];

    calc($op,$num1,$num2);
?>
