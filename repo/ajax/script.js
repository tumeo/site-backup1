$(document).ready(function(){
    $('#somar').click(function(){
        getCalc("+");
    });
    $('#subtrair').click(function(){
        getCalc("-");
    });
    $('#multiplicar').click(function(){
        getCalc("*");
    });
    $('#dividir').click(function(){
        getCalc("/");
    });

});

function getCalc(operador){
    $.post("ajax.php", {"x": $("#x").val(), "y": $("#y").val(), "op": operador})
    .done(function(result){
        $("#resultado").html(result);
    });
}
