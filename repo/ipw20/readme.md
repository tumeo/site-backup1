## Exercícios IPW

>ENTREGA EM 04/08/15 VIA EMAIL (ASSUNTO TRABALHO JS) EM UM ARQUIVO ZIP

>(SEUNOME_2INF.zip) CONTENDO TODOS OS EXERCICIOS SEPARADOS POR PASTA.

`rodrigo@professores.info`


1. Crie uma função JavaScript que, ao clicar com o botão direito na página, aparece amensagem 'Opção desativada', não permitindo, por exemplo, copiar textos ou imagens dessa página.

2. Crie um script que, ao passar o mouse sobre a palavra "verde", a cor da página muda da cor atual (branco neste caso) para verde; se passar sobre a palavra "ouro", a cor muda para amarelo.

3. Faça um código JavaScript que emita uma alert no instante em que uma combobox for marcada e também no momento em que for desmarcada.

4. Faça em java script uma função para habilitar e desabilitar todos os campos de um checkbox, nesse html devera ter 10 checkbox e dois botões, uma para marcar todos e outro para desmarcar todos.

5. Faça usando JavaScript uma função para verificar se as senhas são iguais, imagine um cadastro onde vc vai cadastrar seus dados de login e senha, porém tera dois campos para essa verificação.

6. Faça um javascript que recebe uma data de nascimento e devolve o número de anos desde a data, ou seja, a idade.

7. Elabore um algoritmo que leia dois números inteiros e mostre o resultado da diferença do maior valor pelo menor.

8. Você irá ao McDonalds pedir um Mc'Lanche Feliz de 23,50. Digte o quanto você pagou e mostre o troco. Se você pagar menos mostre uma mensagem dizendo que não foi possível efetuar o pagamento. se for igual ou maior moscar o troco.

9. Receba uma letra e imprima se ela é vogal ou não.

10. Faça um programa que leia 3 números inteiros, e os mostre em ordem crescente.

11. Receba a idade, o sexo e o peso da pessoa, e escreva se essa pessoa pode doar sangue. Para doar precisa ter 18 a 67 anos para ambos os sexos, e mais de 60 kg para mulheres e 65 kg para homens.

12. Faça um script que leia dois caracteres e verifique qual vem primeiro no alfabeto.

13. Faça um programa que informe o mês de acordo com o número informado pelo usuário. (Ex.: Entrada 4. Saída: Abril).

14. Crie um botão em html, que ao ser clicado aparece um número aleatório entre O e 50.

15. Escrever um programa que permita receber o nome e a idade de uma pessoa e em seguida, informar o nome digitado e a idade da pessoa daqui a 30 anos.

16. Faça um programa que leia três notas de um aluno, calcule e imprima a média aritmética entre essas notas.

17. Elaborar um algoritmo que imprima a tabuada de 1 a 9.

18. Crie um algoritmo que imprima a seguinte sequência de valores. O algoritmo deve obrigatoriamente utilizar 2 estruturas de repetição. 9 8 7 8 7 6 7 6 5 6 5 4 5 4 3.

19. Crie um algoritmo que gere o seguinte vetor utilizando laços de repetição: 1,5,9,13,17,21,25,29,33,37. Depois imprima esse vetor.

20. As Organizações Tabajara resolveram dar um aumento de salário aos seus colaboradores e lhe contrataram para desenvolver o programa que calculará os reajustes. Faça um programa que recebe o salário de um colaborador e calcule reajuste segundo o seguinte critério, baseado no salário atual:

    * Salários até R$ 280,00 (incluindo): aumento de 20%
    * Salários entre R$ 280,00 e R$ 700,00: aumento de 15%
    * Salários entre R$ 700,00 e 1500,00: aumento de 10%
    * Salários de R$ 1500,00 em diante: aumento de 5%

    Após o aumento ser calculado, deverá ser informado: o salário antes do reajuste; o percentual de aumento aplicado; o valor do aumento; o novo salário, após o aumento.
