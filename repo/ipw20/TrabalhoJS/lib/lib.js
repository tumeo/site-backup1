var exe = {
    exe1:'1. Crie uma função JavaScript que, ao clicar com o botão direito na página, aparece a mensagem \'Opção desativada\', não permitindo, por exemplo, copiar textos ou imagens dessa página.',
    exe2:'2. Crie um script que, ao passar o mouse sobre a palavra <span class="green">"verde"</span>, a cor da página muda da cor atual (branco neste caso) para <span class="green">verde</span>; se passar sobre a palavra <span class="gold">"ouro"</span>, a cor muda para amarelo.',
    exe3:'3. Faça um código JavaScript que emita uma alert no instante em que uma combobox for marcada e também no momento em que for desmarcada.',
    exe4:'4. Faça em java script uma função para habilitar e desabilitar todos os campos de um checkbox, nesse html devera ter 10 checkbox e dois botões, uma para marcar todos e outro para desmarcar todos.',
    exe5:'5. Faça usando JavaScript uma função para verificar se as senhas são iguais, imagine um cadastro onde vc vai cadastrar seus dados de login e senha, porém tera dois campos para essa verificação.',
    exe6:'6. Faça um javascript que recebe uma data de nascimento e devolve o número de anos desde a data, ou seja, a idade.',
    exe7:'7. Elabore um algoritmo que leia dois números inteiros e mostre o resultado da diferença do maior valor pelo menor.',
    exe8:'8. Você irá ao McDonalds pedir um Mc\'Lanche Feliz de 23,50. Digte o quanto você pagou e mostre o troco. Se você pagar menos mostre uma mensagem dizendo que não foi possível efetuar o pagamento. se for igual ou maior moscar o troco.',
    exe9:'9. Receba uma letra e imprima se ela é vogal ou não.',
    exe10:'10. Faça um programa que leia 3 números inteiros, e os mostre em ordem crescente.',
    exe11:'11. Receba a idade, o sexo e o peso da pessoa, e escreva se essa pessoa pode doar sangue. Para doar precisa ter 18 a 67 anos para ambos os sexos, e mais de 60 kg para mulheres e 65 kg para homens.',
    exe12:'12. Faça um script que leia dois caracteres e verifique qual vem primeiro no alfabeto.',
    exe13:'13. Faça um programa que informe o mês de acordo com o número informado pelo usuário. (Ex.: Entrada 4. Saída: Abril).',
    exe14:'14. Crie um botão em html, que ao ser clicado aparece um número aleatório entre 0 e 50.',
    exe15:'15. Escrever um programa que permita receber o nome e a idade de uma pessoa e em seguida, informar o nome digitado e a idade da pessoa daqui a 30 anos.',
    exe16:'16. Faça um programa que leia três notas de um aluno, calcule e imprima a média aritmética entre essas notas.',
    exe17:'17. Elaborar um algoritmo que imprima a tabuada de 1 a 9.',
    exe18:'18. Crie um algoritmo que imprima a seguinte sequência de valores. O algoritmo deve obrigatoriamente utilizar 2 estruturas de repetição. 9 8 7 8 7 6 7 6 5 6 5 4 5 4 3.',
    exe19:'19. Crie um algoritmo que gere o seguinte vetor utilizando laços de repetição: 1,5,9,13,17,21,25,29,33,37. Depois imprima esse vetor.',
    exe20:'20. As Organizações Tabajara resolveram dar um aumento de salário aos seus colaboradores e lhe contrataram para desenvolver o programa que calculará os reajustes. Faça um programa que recebe o salário de um colaborador e calcule reajuste segundo o seguinte critério, baseado no salário atual:\
        <br><br>* Salários até R$ 280,00 (incluindo): aumento de 20% \
        <br>* Salários entre R$ 280,00 e R$ 700,00: aumento de 15% \
        <br>* Salários entre R$ 700,00 e 1500,00: aumento de 10% \
        <br>* Salários de R$ 1500,00 em diante: aumento de 5% \
        <br><br>Após o aumento ser calculado, deverá ser informado: o salário antes do reajuste; o percentual de aumento aplicado; o valor do aumento; o novo salário, após o aumento.'
};

function getExe(exeN){
    if(!exeN){
        for(var i=1; i <= 20; i++){
            qS('#ipw').innerHTML += '<p><a target="_blank" href="TrabalhoJS/Exe '+i+'/index.html">Exercício '+i+'</a> '+exe['exe'+i]+' </p>';
        }
        qS('#ipw').innerHTML += '<footer><small><a href="https://github.com/williamd1k0" target="_blank">2015 | William Tumeo</a></small></footer>';
    }else{
        qS("h2").innerHTML = exe[exeN];
    }
}

function qS(el){
    return document.querySelector(el);
}

function qA(el){
    return document.querySelectorAll(el);
}

function gClass(el){
    return document.getElementsByClassName(el);
}