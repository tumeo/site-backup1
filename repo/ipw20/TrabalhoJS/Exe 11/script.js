var blood = qA('.blood');
var doar = qS('#doar');
var doar2 = qS('#candoar');

for(var i in blood){
    blood[i].onfocus = function(){
        doar.innerHTML = '<button type="button" name="blood">Doar</button>';
        doar.onclick = function(){
            isDoador(blood[0].value,blood[1].value,blood[2].value);
        };
    };
}

function isDoador(idade, peso, sexo){
    var canDoar = false;
    if((idade >= 18)&&(idade <= 67)){
        if(sexo === 'm'){
            if(peso >= 65){
                canDoar = true;
            }
        }else if(sexo === 'f'){
            if(peso >= 60){
                canDoar = true;
            }
        }
    }
    if(canDoar){
        doar2.innerHTML = '<span class="verde">Pode doar!</span>';
    }else{
        doar2.innerHTML = 'Não pode doar!';
    }
}
