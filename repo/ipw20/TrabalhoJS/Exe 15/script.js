var ipt = qA('input');
var future = qS('#future');

for(var i in ipt){
    ipt[i].onfocus = function(){
        future.innerHTML = '<button type="button" name="future"> Prever futuro </button>';
    };
}

future.onclick = function(){
    getFuture(ipt[0].value,parseInt(ipt[1].value));
};

function getFuture(nome, idade){
    if((nome !== '')&&(idade !== '')&&(!isNaN(idade))){
        idade += 30;
        future.innerHTML = ' '+nome+', daqui há 30 anos você terá '+idade+' anos.';
    }
}
