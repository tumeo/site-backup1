var mcLanche = 23.50;

qS('input').onfocus = function(){
    qS('#pay').innerHTML = '<span> </span><button type="button" name="button">Pagar</button>';
    qS('button').onclick = function(){
        calc();
    }
}

function calc(){
    var result = parseFloat(qS('input').value) - mcLanche;
    if (!isNaN(result)){
        if(result >= 0){
            qS('#pay').innerHTML = '<br><br>Troco<br><input class="result" type="input" name="result" value="'+ result.toFixed(2) +'">';
        }else{
            qS('#pay').innerHTML = '<br><br><i>Não foi possível efetuar o pagamento!</i><br><input class="alert" type="input" name="result" value="'+ result.toFixed(2) +'">'
        }
    }
}
