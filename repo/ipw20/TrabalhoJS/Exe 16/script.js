var ipt = qA('input');
var media = qS('#media');

for(var i in ipt){
    ipt[i].onfocus = function(){
        media.innerHTML = '<button type="button" name="media">Calcular Média</button>';
    };
}

media.onclick = function(){
    calcMedia(ipt[0].value, ipt[1].value, ipt[2].value);
};

function calcMedia(){
    var mediaA = 0;
    for(var i in arguments){
        mediaA += parseFloat(arguments[i]);
    }
    if(!isNaN(mediaA)){
        media.innerHTML = 'Média: '+(mediaA / arguments.length).toFixed(2);
    }
}
