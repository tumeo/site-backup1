var salario = qS('input');
var calc = qS('#calc');
var sAntigo = 0;

salario.onfocus = function(){
    calc.innerHTML = '<button type="button" name="calc">Calcular aumento</button>';
};

calc.onclick = function(){
    sAntigo = parseFloat(salario.value);
    sNovo = calcular(sAntigo);
    if (sNovo){
        calc.innerHTML = sNovo;
    }
};

function calcular(grana){

    if (!((isNaN(grana))&&(!grana))){
        var aumento = 0;
        var percentual = 0;

        if(grana <= 280){
            percentual = 20;
        }else if(grana <= 700){
            percentual = 15;
        }else if(grana <= 1500){
            percentual = 10;
        }else{
            percentual = 5;
        }
        aumento = grana * (percentual / 100);
        grana += aumento;

        return '<br><br>Salário antigo: R$'+sAntigo.toFixed(2)+
        '<br>Percentual de aumento: '+percentual+'%'+
        '<br>Valor do aumento: R$'+aumento.toFixed(2)+
        '<br>Novo salário: R$'+grana.toFixed(2);
    }
    return false;
}
