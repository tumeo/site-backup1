var numMes = qS('input');
var mes = qS('#mes');

numMes.onkeyup = function(){
    checkMonth(this.value);
};

function checkMonth(month){
    if((month >= 1)&&(month <= 13)){
        var mesNome = '';
        switch (month){
            case "1": mesNome = 'Janeiro';
                break;
            case "2": mesNome = 'Fevereiro';
                break;
            case "3": mesNome = 'Março';
                break;
            case "4": mesNome = 'Abril';
                break;
            case "5": mesNome = 'Maio';
                break;
            case "6": mesNome = 'Junho';
                break;
            case "7": mesNome = 'Julho';
                break;
            case "8": mesNome = 'Agosto';
                break;
            case "9": mesNome = 'Setembro';
                break;
            case "10": mesNome = 'Outubro';
                break;
            case "11": mesNome = 'Novembro';
                break;
            case "12": mesNome = 'Dezembro';
                break;
            case "13": mesNome = 'Dezembro (with 13º salário)';
        }
        mes.innerHTML = '<i class="verde">'+month+' equivale a '+mesNome+'</i>';
    }else if(month === ''){
        mes.innerHTML = '';
    }else{
        mes.innerHTML = '<i>Não é um mês válido!</i>';
    }
}
