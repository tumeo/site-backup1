var butt = qS('button');
var tabuada = qS('#tab');

butt.onclick = function(){
    tabuada.innerHTML = '';
    for(var i = 1; i < 10; i++){
        tabGen(i);
    }
    butt.style.display = 'none';
};

function tabGen(tab){
    tabuada.innerHTML += '<br><br><th><i class="verde">Tabuada do '+tab+':</i></th>';
    for(var i = 0; i <= 10; i++){
        tabuada.innerHTML += '<br>'+tab+' x '+i+' = '+ (tab * i);
    }
    tabuada.innerHTML += '<br>---------------------';
}
