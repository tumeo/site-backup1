<?php session_start();

	if(isset($_POST["clear"])){
		setcookie("contatos", '', time() - 3600);
		header("Location: ./");
	}

	if(isset($_POST["new-cont"])){
		if(!isset($_COOKIE["contatos"][0])){
			$last_contato = 0;
		}else{
			$last_contato = sizeof($_COOKIE["contatos"]);
		}
		setcookie("contatos[$last_contato]", $_POST["nome"].'@@'.$_POST["email"].'@@'.$_POST["telefone"]);
		header("Location: ./");
	}

?>

<html>
<head>
	<meta charset="utf-8">
	<title>Desafio 3</title>
</head>
<body>

	<div id="new-contato">
		<h2>Novo contato</h2>
		<form method="post" action="./">
			<input type="hidden" name="new-cont" value="1">
			<ul>
				Nome:
				<input type="text" name="nome" placeholder="Digite o nome" required>
			</ul>

			<ul>
				E-mail:
				<input type="email" name="email" placeholder="Digite o email" required>
			</ul>

			<ul>
				Telefone:
				<input type="tel" name="telefone" placeholder="Digite o telefone" required>
			</ul>

			<button type="submit">Cadastrar</button>
		</form>

		<form action="./" method="post">
			<input type="hidden" name="clear" value="1">
			<button type="submit" name="clear-all">Limpar</button>
		</form>
	</div>

	<div id="contatos">

		<?php

			if(isset($_COOKIE["contatos"][0])){
				#var_dump($_COOKIE["contatos"]);
				$contatos = array();
				for($i = 0; $i < sizeof($_COOKIE["contatos"]); $i++){
					array_push($contatos, split("@@",$_COOKIE["contatos"][$i]));
					#print_r($contatos);
				}
				?>
				<table border="1">

					<thead>

						<th>Nome</th>
						<th>Email</th>
						<th>Telefone</th>

					</thead>

					<tbody>
						<?php for($i = 0; $i < sizeof($contatos); $i++){ ?>
						<tr>
							<td><?php echo $contatos[$i][0]; ?></td>
							<td><?php echo $contatos[$i][1]; ?></td>
							<td><?php echo $contatos[$i][2]; ?></td>
						</tr>

						<?php } ?>

					</tbody>

				</table>
		<?php

			}

		?>

	</div>

</body>
</html>
<?php #session_destroy(); ?>
