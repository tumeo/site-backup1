# É necessário um bloco init para criar as variáveis antes de inicializar o resto.
init python:
    # variáveis globais, serão alteradas dinamicamente.
    vidaPlayer = 100
    vidaMaxima = 100

# Criando uma tela (screen) -> uma screen pode ser criada em qualquer lugar, inclusive no arquivo screens.rpy
# Telas são layers do jogo que podem ser exibidas acima do layer principal.
# Screens suportam argumentos, como em um método, no caso você pode passar informações quando exibir essa tela.
screen barraDeVida(tamanhoDaBarra=10):

    # Aqui fica a linguagem básica usada em screens.
    # Frames são subdivisões em uma tela.
    frame:

        # Vbox é uma caixa visual com orientação vertical.
        has vbox
        # Uma referência visual, poderia ser uma imagem também.
        text "Vida"

        # A barra em si, que vai usar variáveis para definir os valores.
        # vidaMaxima(global::inteiro): o número máximo pra vida.
        # vidaPlayer(global::inteiro): o número da vida atual do personagem.
        # tamanhoDaBarra(local::inteiro ou flutuante): o tamanho da barra, se for
        # inteiro então é passado um valor em pixel, se flutuante então é em porcentagem.
        $ ui.bar(vidaMaxima,vidaPlayer,xmaximum=tamanhoDaBarra)

        # Modelo para uma bara vertical:
        # $ ui.bar(vidaMaxima,vidaPlayer,ymaximum=tamanhoDaBarra,style="vbar")

# Declaração do personagem
define personagem = Character("Mocinho")


# Início do jogo
label start:
    # exibe a barra de vida sem passar nenhuma informação
    show screen barraDeVida() # sem argumento, o tamanho será o pré-definido (100)

    # Characters são objetos e podem guardar atributos
    $ personagem.vida = 100

    personagem "Sinto uma presença!"
    # oculta a barra de vida
    hide screen barraDeVida

    personagem "Hmmm...."

    # novamente mostrando a barra, mas agora com argumento
    show screen barraDeVida(500) # agora o tamanho da barra vai ser alterado para 500
    narrator "Um monsto misterioso ataca!"

    # diminuindo o atributo "vida" de "personagem" em 50
    $ personagem.vida -= 50
    # repassando o valor da vida do personagem para a variável global responsável por exibir na barra
    $ vidaPlayer = personagem.vida

    # checar se a vida "global" é menor que zero (aka morreu)
    call checarVida
    # se for maior que zero continua
    personagem "Ai!! Essa machucou~"
    narrator "O monsto misterioso ataca novamente!"

    # alterando novamente a vida e atualizando na barra
    $ personagem.vida -= 50
    $ vidaPlayer = personagem.vida

    # checando a vida novamente, mas agora vai dar zero
    call checarVida
    # apartir daqui nada será executado pois o personagem morre
    personagem "Sou um fantasma agora..."


# label que faz a checagem da vida
label checarVida:
    # se a vida for igual ou menor que zero
    if vidaPlayer <= 0:
        centered "Sua vida esgotou!"
        hide screen barraDeVida
        centered "GAME OVER"
        # este método força a execução para o menu princiapal
        $ renpy.full_restart()
    else:
        # se não for menor que zero só retorna
        return
