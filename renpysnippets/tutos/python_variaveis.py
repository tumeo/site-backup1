### Básicos de variáveis

## Variáveis: São informações/dados que você guarda em espaços reservados na memória RAM, simples assim.
# Em linguagens de baixo nível (quando você trabalha diretamente com a máquina), você precisa
# preencher esses espaços da RAM manualmente, trabalhando com binários, hexadecimal etc.
# Em linguagens de médio e alto nível (quando você trabalha em uma interface mais humana), você
# usa "links" para esses espaços. Esses links apontam para os espaços na memória.
# É que nem uma url da internet, vc digita www.google.com e o servidor DNS interpreta e aponta
# para o IP do servidor. Variáveis são assim também, elas possuem uma "url", que seria o nome
# desse link. E quando você manipula essa variável (inserindo ou obtendo dados da RAM), o interpretador
# da linguagem aponta automaticamente para o espaço específico na RAM.

# Um exemplo de um simples armazenamento de uma variável:
# Vamos supor que você queira guardar seu nome na memória Ram.
# meu_nome = "Seu nome"
#    [1]  [2]   [3]
# 1: Esse é o nome da variável, o link que você usa para acessá-la.
# 2: Um sinal de igual serve para indicar a atribuição.
# 3: O valor da variável, ele que vai ficar salvo na RAM.
# Esse padrão pode variar de linguagem pra linguagem, mas sempre vai seguir a base desse padrão:
# [NOME DA VARIÁVEL] [RECEBE] [VALOR DA VARIÁVEL]

## Nomes: Nomes de váriáveis devem seguir um padrão pré-estabelecido, isso porque alguns nomes
# não são permitidos e podem gerar erros.
# Primeiramente, a maioria das linguagens não suporta espaço, se o nome da sua variável tiver mais
# de uma palavra, você precisa usar caracteres especiais (geralmente underscore, tipo_desse_jeito) ou usar um padrão
# diferente, como o mais usado "camelCase" (que é escrever começando com minúscula e capitalizando
# outras palavras, tipoDessaForma).
# Cuidar com os caracteres especiais, evitar ao máximo pois boa parte deles são usados para operações
# matemáticas entre outras coisas, exemplo: + - / * \ $ # % & | ( ) { } [ ] ! " " entre outros...
# Esses caracteres reservados vão variar dependendo da linguagem.
# Nunca COMEÇAR o nome de uma variável com números, isso dá conflitos por causa das operações matemáticas.
# Exemplo: 1numero = 123 <- Errado
#          numero1 = 123 <- Certo
# Nunca usar palavras reservadas da linguagem como nome de variável. Essas palavras são usadas para
# o funcionamento básico da linguagem e você conhece essas palavras aos poucos.

## Tipos de variáveis: As variáveis são identificadas por tipos. Os tipos variam muito de linguagem
# pra linguagem, mas os tipos mais básicos são Números, Strings e Lógicos.

# Strings: são as variáveis de texto, usam aspas duplas ou simples.
# Exemplos simples:
um_texto = "Olá mundo"
outro_texto = 'Olá mundo'

# Estando dentro de uma string simples em python, não é possível quebrar linhas, geraria um erro.
texto_quebrado = "Olá mundo,
como vai você?"

# Por outro lado, em Ren'py isso é nativamente suportado.
# Em python, strings multi-linha são usadas da mesma forma que em comentários multi-linha.
texto_quebrado = """
Uma string
de várias
linhas
"""

# Números: são as variáveis de números, sendo geralmente elas inteiros ou decimais.
# Exemplos simples de números inteiros:
minha_idade = 21
vidas_do_jogador = 5

# Números decimais em python são descritos com "ponto" e não com "vírgula", como ocorre na maioria das linguagens.
# Em programação, números decimais são chamados de números com ponto flutuante (float em inglês).
# Exemplos simples de números decimais (float):
minha_altura = 1.74
meu_saldo_no_banco = 0.00

# Lógicos: são variáveis que só podem ter 2 valores, verdadeiro ou falso.
# Essas variáveis, conhecidas como boolean, servem para fazer o controle de coisas.
# Tanto em python como no renpy, os booleanos começam com letra maiúscula: True (verdadeiro) ou False (falso).
# Exemplos simples de variáveis booleanas:
eh_de_noite = True
tem_dinheiro = False

## Existem mais tipos específicos em python, só que mais complexos (vou deixar separadamente),
## as listas, os dicionários e os objetos.
