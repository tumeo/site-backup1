### Básicos de Python

## Comentários: Comentários são uma das coisas mais importantes na programação.
# Comentários são trechos ou blocos de um código que são ignorados na hora da execução.
# Eles servem para você detalhar as coisas, documentar e as vezes para testar o seu código.

# O caractere "#" (hashtag) inicia um comentário de uma linha.
# Ou seja, tudo que está escrito aqui será ignorado pelo interpretador!

# Comentários em Python tem suporte a multi-linha usando 3 aspas para início e 3 para o fim.
"""Exemplo:
Um comentário de várias linhas
será ignorado da mesma forma
que o comentário de uma linha.
"""
# Lembrando que a linguagem Ren'py não suporta nativamente os comentários multi-linha.

## Estrutura básica: Python é uma linguagem muito versátil e de sintaxe muito limpa.
# Ela tem dois modos de distribuição, script e compilada.
# O modo script é a distribuição dos source-codes (.py), que podem ser auto-executados.
# O modo compilado distribui só o bytecode (linguagem intermediária entre o código e a máquina).

# Python tem uma estrutura muito versátil, podendo ser estruturada, funcional e orientada a objetos.
# Por padrão, se aprende primeiro a estruturada, e python facilita bastante nessa parte.
# Isso porque não é nem preciso criar um arquivo para testar isso, basta usar o modo interativo
# do Python, tanto no CLI (terminal/prompt) quanto no programa IDLE que já vem junto.

## Python Estruturado: Trabalhando de forma estruturada, um código vai ser obrigatoriamente lido
# de cima para baixo sem uma alteração drástica no fluxo do programa, como numa receita de bolo simples.
# Antes de entender variáveis e outros conceitos da linguagem, você precisa entender as operações
# matemáticas básicas.

## Operadores matemáticos: São basicamente os operadores que você aprendeu no ensino fundamental.
# Somar: usa o caractere + (mais/cruz)
# Subtrair: usa o caractere - (menos/hífen/tracinho)
# Multiplicar: usa o caractere * (asterisco)
# Dividir: usa o caractere / (barra -> não confundir com barra invertida "\")
# Resto/Módulo: usa o caractere % (por cento) -> o módulo é o resto de uma divisão.
    # Ex1: 8 dividido por 2 dá 4, e o resto é zero pois a divisão foi inteira.
    # Ex2: 5 dividido por 2 dá 2.5, e o resto é 1 pois a divisão foi "quebrada".
# Separador: usa os caracteres de parênteses ( ) -> isso serve para as prioridades nas operações.
    # Ex1: 1 + 2 * 5 = 11  <- multiplicações e divisões são executadas primeiro.
    # Ex2: (1 + 2) * 5 = 15 <- o que tiver dentro dos parênteses é executado primeiro.

# Outros operadores podem ser usados depois que você entender o conceito das variáveis.
