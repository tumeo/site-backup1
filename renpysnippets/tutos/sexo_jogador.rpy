## Bloco Python para criar uma Classe
# - Dependendo do tamanho pode ficar em outro arquivo
# - Uma classe é basicamente um contrutor (molde) para um objeto.
# - Um objeto precisa ser instanciado, como um Character por exemplo.
# - Ex: define lorem = Character("Lorem Ipsum")
#        [*]    [1]             [2]
# 1 = Objeto instanciado.
# 2 = Classe Character.
# * = A palavra-chave "define" é uma mistura de "init + $".
# "init" = Bloco que é executado antes de iniciar o jogo.
# "$" = Palavra-chave para usar uma expressão Python em uma unica linha.
# "python" = Bloco que é usado para usar somente expressões Python.
init python:
    ## Bloco da classe Genero
    class Genero:
        ## Método contrutor da classe
        # - Um método contrutor é executado quando um objeto é instanciado
        # - o argumento "sexo" será passado no instanciamento
        # - Ex: $ sexo = Genero("m")
        # - o objeto sexo irá receber todos os atributos masculinos(m)
        def __init__(self, sexo):
            # "self" é usado para referenciar a própria classe.
            # Usando o mesmo exemplo acima, os atributos podem ser acessados por "ponto":
            # sexo.genero
            # sexo.artigo
            self.genero = sexo

            # Abaixo vem a verificação se é masculino("m") ou feminino("f").
            # Entrando em cada bloco, serão definidas palavras específicas pra cada sexo.
            # Usando o exemplo do artigo, o masculino usa "o" e o feminino "a"
            if sexo == "m":
                self.artigo = "o"
            elif sexo == "f":
                self.artigo = "a"
# Iníco do jogo
label start:
    # um simples menu para escolher o sexo.
    menu escolha_sexo:
        "Escolha o seu sexo!"
        "Masculino":
            # Se Masculino, intancia o objeto passando o parâmetro "m"
            $ sexo = Genero("m")
        "Feminino":
            # Se Feminino, intancia o objeto passando o parâmetro "f"
            $ sexo = Genero("f")

    # Este bloco python serve para poder usar o comando while para criar
    # um loop na hora de digitar o nome do jogador (tentativas)
    python:
        # Tentativa de obter o nome
        jogador = renpy.input("Olá, qual é seu nome: ")
        # Número de tentativas caso entre com um valor em branco.
        tentativas = 2

        # Este loop obriga o jogador a entrar com o nome usando o número de tentativas.
        # Caso queira obrigar sem número de tentativas, somente use o [ jogador == "" ]
        while jogador == "" and tentativas > 0:
            # Diminui a quantidade de tentativas a cada repetição.
            tentativas -= 1
            # Avisa a entrada inválida e pede novamente.
            centered("Entrada inválida!")
            jogador = renpy.input("Qual é seu nome: ")

            # Se a entrada estiver vazia, checa o número de tentativas restantes.
            if jogador == "":
                # Se for zero, jogador receber None (nada/nenhum) e sai do loop.
                if tentativas == 0:
                    jogador = None
            # Se a entrada não for vazia (nome inserido), zera as tentativas e sai do loop.
            else:
                tentativas = 0
    # Fora do bloco python, faz uma checagem do nome do jogador.
    # Se não existir um nome (None), um nome padrão é definido com base no sexo escolhido.
    if not jogador:
        # Usando o atributo "genero" do objeto criado acima para checar.
        if sexo.genero == "m":
            $ jogador = "João"
        else:
            $ jogador = "Maria"

        # Uma mensagem para mostrar o nome padrão definido.
        centered "Por falta de escolha, o nome será [jogador]"

    # Uma mensagem exemplo para usar os atributos do objeto.
    # Se for masculino, bem-vind[sexo.artigo] vira bem-vindo
    # Se for feminino, bem-vind[sexo.artigo] vira bem-vinda
    narrator "Seja bem-vind[sexo.artigo], [jogador]!"

    # Usando esse processo com classes, é possível personalizar várias palavras que precise.
    return
