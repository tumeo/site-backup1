# -*- encoding: utf-8 -*-

## Controles de fluxo
# Fluxo é a vida de um código, literalmente.
# Da mesma forma que você está lendo este texto de cima para baixo, um código tem basicamente
# o mesmo fluxo que um script teatral, por isso que esse tipo de código se chama script mesmo.

# Isso se chama programação estruturada, onde você segue essa estrutura "de cima pra baixo".
# Mas diferente de uma simple peça teatral, um script possui algumas variações no seu fluxo.
# O primeiro tipo mais importante dessa variação é o controle por decisões.

# Isso é como uma escolha qualquer, como qualquer outra escolha que você faz na vida.
# Para facilitar, vamos ir direto para a prática, criando um script que calcula a idade de
# uma pessoa da forma mais simples possível.

## Cálculo de idade:
# Antes de ir para a parte de controle, vamos botar em prática os conceitos anteriores.

# Uma mensagem simples somente pra pessoa saber do que se trata:
print "-- Cauculo de idade --"

# Primeiro precisamos saber em que ano estamos
# Pra isso vamos usar uma variável do tipo inteiro
ano_atual = int(raw_input("Em que anos estamos? "))

# Agora precisamos saber em que ano a pessoa nasceu
# Outra variável do tipo inteiro
ano_nascimento = int(raw_input("Em que ano voce nasceu? "))

# Para calcular a idade iremos usar os operadores matemáticos, subtraindo
# o ano de nascimento do ano atual e atribuindo a outra variável
idade = ano_atual - ano_nascimento

# Me usando como exemplo, nascendo em 1994, e subtraindo de 2015, obtemos 21.
# Então vamos mostrar o resultado, usando substituição por chaves
print "Em {}, voce fara {} anos.".format(ano_atual,idade)
# Esse raw_input vazio é só para forçar uma pausa na execução
raw_input()

# Até aí você vê que o fluxo seguiu sem nenhuma variação.
# Agora vamos continuar com o mesmo programa para explicar os controles.
# Aumentando a história, agora o programa também reconhece se a pessoa é de maior ou não.

# Escrevendo de forma normal, em português, temos o seguinte o seguinte:
"""
SE a idade da pessoa for MAIOR OU IGUAL a 18 anos, ENTÃO diga que a pessoa é maior de idade.
SENÃO, ela é menor de idade.
"""

# Repare que alguns termos estão destacados ali, eles vão ser muito importantes nessa comparação.
# Primeiramente nós temos os operadores relacionais, que servem para representar uma relação entre
# valores, variáveis ou expressões.

## Operadores relacionais: são os operadores primitivos para fazer comparações.
# Esses operadores são muito parecidos com aqueles que usamos na matemática:
# Maior que: representado pelo angular de maior ">"
# Menor que: representado pelo angular de menor "<"
# Maior ou igual a: representado pelo angular de maior ">" junto com o sinal de igual "="
# Menor ou igual a: representado pelo angular de menor "<" junto com o sinal de igual "="
# Igual a: representado por dois sinais de igual "==", não confunda com o "=", que serve para atribuição
# Diferente de: representado por um sinal de exclamação "!" junto com um sinal de igual "="

# Como esses operadores servem para comparação, eles vão gerar algum tipo de resultado.
# Da mesma forma que o resultado de 1 + 1 é 2, só que comparações só podem ter 2 resultados possíveis.
# Os resultados possíveis são VERDADEIRO ou FALSO, que são os valores lógicos (booleanos).

# Usando o exemplo da maioridade, é preciso comparar se um número é maior ou igual a 18.
# Sendo assim, vamos exibir somente esse resultado, usando o operador de "maior ou igual".
# Repare que irá mostrar True se for de maior e False se for de menor.
print idade >= 18
raw_input()

# Um pequeno adendo, por que utilizar o operador de "maior ou igual" ao invés do "maior"?
# Isso pode ser confuso no início, mas o operador de "maior" não conta o número que é comparado.
# Logo, se a idade fosse 18, ele só iria dar verdadeiro caso fosse maior, poderia ser até 18.00001, mas nunca o 18.
# Então diz-se maior ou igual porque você quer incluir o 18 na comparação.
# Exemplos:  idade 19 - é maior que 18 (True)
#            idade 18 - não é maior que 18 mas é igual (True)
#            idade 17 - não é nem maior nem igual a 18 (False)

# Agora que você já sabe como comparar valores, podemos fazer o controle deles.

## Controle simples de fluxo: Essa é a outra parte destacada do texto mais acima.
# Ela é composta pelos SE e SENÃO.

# Já adiantando, os equivalentes a isso em inglês são "if" e "else", respectivamente.
# Antes de fazer o comparativo, é preciso ter a noção dos blocos em python.

## Blocos: são delimitações dentro de um código, como o exemplo acima:
# SE for de maior, então diga que é de maior.
#               [1]                         [2]

# [1] - Começa o bloco
# [2] - Termina o bloco.
# Então, tudo que está dentro desse bloco só vai acontecer caso a pessoa seja de maior.

# Em Python, um bloco comum é delimitado por tabulação, que é exatamente um TAB (a tecla tab),
# ou o equivalente, que seriam espaços.
# Então vamos ver como isso fica na prática já seguindo com o mesmo script da idade.

# Faz a comparação da idade
# SE idade for maior ou igual a 18 então..
# (o símbolo de dois pontos ":" sempre abre o bloco)
if idade >= 18:
    # Repare que essa linha está mais a frente que a de cima, ela tem um tab (ou espaços)
    print "Voce e maior de idade!"
    # Esse espaçamento se chama indentação
    # Quando esse espaçamento acabar, o bloco termina

# Agora essa linha não tem o tab, isso quer dizer que está fora do bloco
# O SENÃO é a sobra, tipo um plano B
# Logo, se a pessoa não é de maior, é de se esperar que seja de menor.
else:
    # Mair uma vez usando a indentação pra definir o bloco
    print "Voce e menor de idade!"

# Como a linha seguinte está fora de qualquer bloco, vai ser executada independente se é ou não de maior.
print "Fim do Programa"
raw_input()

# Experimente tirar a indentação dos blocos pra testar, não vai nem executar o script.
# Experimente também refazer essa parte, só que invertendo, verificando se a pessoa é
# de menor primeiro.
