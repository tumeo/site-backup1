# -*- encoding: utf-8 -*-

## Comandos de entrada e saída
# Operações de entrada e saída (IO - input/output) podem ser qualquer coisa que,
# como diz o nome, entram no programa ou saem.

# Os mais básicos são usados no prompt mesmo.
# Para "imprimir" um simples texto na tela, é usado o que na programação é chamado
# de método ou função (será explicado mais pra frente), que seria o método "print".
# O método print recebe um texto e imprime na tela, seguindo o exemplo:

# Isso irá imprimir na tela "Hello, world" sem as aspas.
# É importante fazer isso porque é uma lei universal imprimir um "Hello, world" sempre
# que você aprende uma nova linguagem!
print "Hello, world!"

# Supondo que você já estudou sobre as variáveis, vamos supor que você quisesse imprimir uma.
# Por exemplo:
# Criando a variável
meu_nome = "William"
# Imprimindo na tela
print meu_nome

# Isso vai simplesmente imprimir "William", mas não tem nenhum contexto isso.
# Vamos imprimir isso no meio de uma frase. Para isso vamos usar o que chamamos de concatenação.
# Concatenação é o ato de juntar strings literais com variáveis.
# Isso varia muito de linguagem pra linguagem, e em python existem 3 formas de se fazer isso.

# Concatenação com mais (+): Utilizando + entre as strings e as variáveis.
# A vantagem é que é simples de usar, e a desvantagem é que é obrigado a converter
# não-strings para string, caso contrário vai gerar erro. Exemplo:

# Variável do tipo inteiro
minha_idade = 21

# concatenando e imprimindo
print "Oi, meu nome e " + meu_nome + "!"

# Agora a idade, como é um número, vai gerar um erro.
print "Eu tenho " + minha_idade + " anos!"

# Isso ocorre porque Python é uma linguagem fortemente tipada.
# Em algumas outras linguagens o número seria automaticamente convertido para string.
# O ato de converter variáveis de um tipo para o outro é chamado de casting.
# Para converter você usa os métodos específicos, mas bem simples.

# Convertendo para string:
nova_string = str(2015)
# nova_string agora é a string "2015"

# Convertendo para inteiro:
novo_numero = int("2014")
# novo_numero agora é um número inteiro, 2014.
# isso é importante principalmente se você precisar fazer operações matemáticas, por exemplo.

# Convertendo para decimal:
novo_float = float("156.90")
# Se você tentar converter uma string para inteiro e for um número decimal, vai gerar erro.
# Por ser fortemente tipado, você é obrigado a usar o método específico para floats.

# Agora comente a linha do erro (39) e use a forma já convertendo o número para string.
print "Eu tenho " + str(minha_idade) + " anos!"

# Agora juntando tudo
print "Meu nome e " + meu_nome + " e eu tenho " + str(minha_idade) + "."

# Concatenação com por cento (%): Esse método usa a substituição como base.
# Você insere o símbolo seguido do tipo da conversão (esse método converte automaticamente).
# Os tipos de conversão são letras, exemplo "s" para string.
# O símbolo é inserido no meio da string, e as variáveis são inseridas no fim. Exemplo:

# O "%s" vai ser substituído pelo que tiver no fim, no caso "meu_nome"
print "Oi, meu nome e %s !" % meu_nome

# Imprimindo a frase toda, ele segue a ordem de escrita para mais de um "%"
print "Meu nome e %s e eu tenho %s." %(meu_nome, minha_idade)

# Existem outros tipos de "%" que não vale a pena explicar ainda.

# Concatenação com chaves ({}): Também usa substituição, mas de uma forma diferente.
# Você usa as chaves no meio da string e no fim usa um método para formatar.
# Lembrando que esse método converte número automaticamente.
# Este método se chama "format" justamente porque é mais fácil para formatar o texto.
# Exemplo simples:

# Só uma variável
print "Meu nome e {}.".format(meu_nome)

# Mais de uma variável
print "Meu nome e {} e eu tenho {}.".format(meu_nome, minha_idade)

# É possível personalizar basicamente de duas formas:

# Usando ordem numérica
print "Meu nome e {1} e eu tenho {0}.".format(minha_idade, meu_nome)

# Usando variáveis temporárias
print "Meu nome e {nome} e eu tenho {anos}.".format(anos=minha_idade, nome=meu_nome)

""" Atenção: Esses métodos de concatenação são para Python, e não Ren'py!
Isso porque em Ren'py é usado um método nativo da engine e as chaves {} são
usadas para outro tipo de formatação.
Farei um tópico mais específico sobre concatenação em Ren'py pra não confundir.
"""

# Por enquanto você só sabe imprimir coisas que foram escritas direto no código.
# Mas nem toda pessoa vai se chamar William e ter 21 anos, então isso precisa ser
# diferente para cada pessoa que executa o programa.
# E é claro que a pessoa não vai ter que mexer com variáveis, ela vai ter que inserir
# esses valores na interface interativa ("gráfica").

# Para entrar com valores usamos um método que espera o usuário digitar valores e depois
# ele retorna o valor para a variável. Pra isso usamos o método "raw_input".
# Esse método, além de receber os valores, também imprime uma mensagem.
# Um exemplo bem simples:

# A variável meu_nome vai receber o retorno (lembre bem da palavra "retorno") do método
meu_nome = raw_input("Digite o seu nome: ")

# Agora a idade
minha_idade = raw_input("Digite a sua idade: ")

# Agora imprimindo
print "Meu nome e " + meu_nome + " e eu tenho " + minha_idade + "."
# Repare que dessa vez não foi convertido a idade para string.
# Isso acontece porque o método raw_input só recebe string.

# Existe outro método de entrada simples, mas que funciona de uma forma totalmente diferente.
# É o método "input", ele faz basicamente a mesma coisa que o raw_input, porém ele não
# recebe string por padrão, ele recebe expressões python.
# Ou seja, se você quiser entrar com uma string, precisa digitar ela com aspas e um número
# será reconhecido como um número mesmo.
# Se você digitar uma palavra sem aspas, ele irá reconhecer como uma variável e irá gerar
# erro caso a variável não exista.
# Esse método só é usado em casos especiais então deixe de lado por hora.

# Exemplo simples (experimente inserir com e sem aspas para ver o que ocorre):
meu_nome = input("Digite o seu nome: ")

# Essa é a forma mais básica de IO em python, em seguida irei escrever sobre controles de fluxo,
# que são os SE e SENÃO.
