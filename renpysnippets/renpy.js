var path = "tutos/",
    code = document.querySelector("code"),
    titulo = document.querySelector("h3"),
    search = window.location.search,
    match = search.match("\\?snippet="),
    scripts = {
        "barra_de_vida.rpy":["Barra de vida", "barra_de_vida.rpy"],
        "sexo_jogador.rpy":["Sexo personalizável", "sexo_jogador.rpy"],
        "python_lang.py":["Básicos de Python", "python_lang.py"],
        "python_variaveis.py":["Variáveis em Python", "python_variaveis.py"],
        "python_io.py":["Input e Output em Python", "python_io.py"],
        "python_fluxos.py":["Controle de fluxo em Python", "python_fluxos.py"]
    };

if(match == null){
    listScripts();
}else{
    snippet = search.replace(match,"");
    if(scripts.hasOwnProperty(snippet)){
        getScript(snippet);
    }else{
        alert("Arquivo não encontrado!");
        location.href = "/renpysnippets/";
    }
}

function listScripts(){
    document.querySelector('html').style.visibility = "visible";
    lista = document.getElementById('lista');
    for(var i in scripts){
        lista.innerHTML += `<a class="lista" href="?snippet=${scripts[i][1]}">
                                ${scripts[i][0]}
                            </a><br>`;
    }
}

function getScript(fileName){
    $.ajax({
        url: path+fileName,
        success: function(fileText) {
            code.innerHTML = fileText;
            titulo.innerHTML = scripts[fileName][0];
            loadCode();
        },
        error: function() {

        }
    });
}

function loadCode(){
    (function() {
        var pre = document.getElementsByTagName('pre'),
            pl = pre.length;
        for (var i = 0; i < pl; i++) {
            pre[i].innerHTML = '<span class="line-number"></span>' + pre[i].innerHTML + '<span class="cl"></span>';
            var num = pre[i].innerHTML.split(/\n/).length;
            for (var j = 0; j < num; j++) {
                var line_num = pre[i].getElementsByTagName('span')[0];
                line_num.innerHTML += '<span>' + (j + 1) + '</span>';
            }
        }
    })();
    setTimeout(function(){
        hljs.initHighlighting();
        document.querySelector('html').style.visibility = "visible";
    },100);
}
