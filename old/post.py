
from datetime import datetime

post_template = """---
layout: post
title: "{name}"
l10n: {local}
date:   {year}-{month}-{day} {hour}:{minute}:{second} -0300
categories: projects
tags: [tag1, tag2]
image_top: image.png
---
<h1>{{% include l10n-post.html post="{local}" index=1 %}}</h1>

"""

localization_template = """
- lang: pt
  terms:
    - {title}
    - None

- lang: en
  terms:
    - {title}
    - None

"""

def zero(n, size=2):
    ns = str(n)
    while len(ns) < size:
        ns = '0'+ns
    return ns

if __name__ == "__main__":
    date = datetime.today()
    stamp = date.year, zero(date.month), zero(date.day), zero(date.hour), zero(date.minute), zero(date.second)

    name = input("> Post name: ")
    local = input("> Localization id: ")
    title = name.replace(" ", "-").lower()
    
    filename = "{}-{}-{}-{}.html".format(stamp[0], stamp[1], stamp[2], title)
    post_body = post_template.format(
        name=name,
        local=local,
        year=stamp[0],
        month=stamp[1],
        day=stamp[2],
        hour=stamp[3],
        minute=stamp[4],
        second=stamp[5]
    )
    
    with open("_posts/"+filename, 'w', encoding="utf-8") as post:
        post.write(post_body)

    with open("_data/posts/"+local+".yml", 'w', encoding="utf-8") as localfile:
        localfile.write(localization_template.format(title=name))
