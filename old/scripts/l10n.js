var lang;
var def_lang = document.querySelector('meta[name="lang"]').content;

if (localStorage.lang === undefined) {
    localStorage.setItem('lang', 'en');
    lang = def_lang;
}else{
    lang = localStorage.getItem('lang');
}

function change_language(or, lg) {
    $('span[lang="'+or+'"]').hide();
    $('span[lang="'+lg+'"]').show();
}

function set_language(lg) {
    if (lg !== lang){
        change_language(lang, lg);
        lang = lg;
        localStorage.setItem('lang', lg);
    }
}

window.addEventListener('DOMContentLoaded', function(event){
    if (lang !== def_lang){
        change_language(def_lang, lang);
    }
    $('a.set-lang').click(function(event){
        event.preventDefault();
        set_language(this.getAttribute('lang'));
        return false;
    });
});
